#!/bin/sh

arq=`echo $1 | sed s/"\.tex"//g`

latex $arq.tex

bibtex $arq.aux

makeindex $arq.idx

latex $arq.tex

latex $arq.tex

dvipdft $arq.dvi

open -a Preview $arq.pdf
