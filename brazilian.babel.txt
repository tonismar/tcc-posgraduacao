%
% defini��es para textos em portugu�s
%
\@namedef{captionsbrazilian}{%
	\def\bibname{Refer{\^e}ncias}%
	\def\abstractname{Resumo}%
	\def\appendixname{Ap{\^e}ndice}%
	\def\contentsname{Sum{\'a}rio}%
	\def\listfigurename{Lista de Figuras}%
	\def\listtablename{Lista de Tabelas}%
	\def\figurename{Figura}%
	\def\tablename{Tabela}%
	\def\advisorname{Orientador}%
	\def\coadvisorname{Co-orientador}%
	\def\listabbrvname{Lista de Abreviaturas e Siglas}%
	\def\listsymbolname{Lista de S{\'\i}mbolos}%
	\def\annexname{Anexo}%
	\def\keywordsname{Palavras-chave}%
	\def\byname{por}%
	%\def\dissname{Disserta{\c{c}}{\~a}o (mestrado)}%
	\def\dissname{Monografia (gradua{\c{c}}{\~a}o)}%
	\def\dissspecificinfo{%
		Trabalho de conclus{\~a}o apresentado {\`a} banca examinadora do curso de Ci{\^e}ncia da Computa{\c{c}}{\~a}o do Centro Universit{\'a}rio La Salle - Unilasalle, como exig{\^e}ncia parcial para obten{\c{c}}{\~a}o do grau de
 Bacharel em Ci{\^e}ncia da Computa{\c{c}}{\~a}o, sob orienta{\c{c}}{\~a}o da \@advisor.
	}%
	\def\coordname{Coordenador do Curso}%
	\def\coursecs{Curso de Bacharelado em Ci{\^e}ncia da Computa{\c{c}}{\~a}o}%
}
\@namedef{datebrazilian}{%
	\def\monthname{%
		\ifcase\month\or%
			janeiro\or fevereiro\or mar{\c{c}}o\or abril\or%
			maio\or junho\or julho\or agosto\or setembro\or%
			outubro\or novembro\or dezembro%
		\fi%
	}%
	\def\ii@date{\@publmonth\ de~\@publyear}%
}
